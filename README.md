#test-hsf
本工程以最原始的Servlet为基础构件，支持通过Http触发对开发环境HSF Provider测试，其自身相当于Consumer端应用：  
> 提前说一句：由于启动HSF必须依赖Ali-tomcat，所以每次单元测试的时候，必须启动这样一个web工程来通过http触发consumer，进行测试，感觉这个远远不如目前Hessian通过Junit测试来的优雅，所以单元测试更好的方式还在尝试中，大家有更好的建议也欢迎提出来~


### 环境搭建
- Provider与Consumer应用均要求通过Ali-tomcat进行运行，配置方式详见[EDAS开发手册](http://EDAs-public.oss-cn-hangzhou.aliyuncs.com/doc/EDAS_DEVELOPER_GUIDE.pdf?spm=5176.7946893.222623.7.k2GuuY&file=EDAS_DEVELOPER_GUIDE.pdf)
- 需要依赖开发环境内网中的一台EDAS配置中心，然后进行host映射 **${配置中心IP} jmenv.tbsite.net**，好像配置中心不支持外网IP，所以要求开发者和配置中心在一个内网

### 操作方式
1. Provider工程，首先对完成对api与business的相关开发
2. Provider工程的hsf模块用Ali-tomcat进行部署，直接在hsf模块上右击选择Run Configuration，使用Ali-tomcat启动操作方式请详情参考上述的开发手册
  
3. 然后将工程中配置成需要测试的Consumer类，可以参考已有的案例  
`
    <hsf:consumer 
    id="userInfoService" 
    interface="com.dingsns.start.user.intf.UserInfoService" 
    version="1.0" 
    group="start-user" />
`
4. 同样用Ali-tomcat启动本工程
5. 通过URL触发单元测试，比如"http://localhost:8888/userInfoTest"

请注意**如果同一台机器开启多个Ali-tomcat，注意加入JVM配置，以免端口冲突:** <font color="#000FF">-Dhsf.server.port=12277</font>或者Ali-tomcat的run configuration里面也有hsf port的参数指定

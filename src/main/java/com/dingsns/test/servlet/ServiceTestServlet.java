package com.dingsns.test.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dingsns.start.user.intf.UserInfoService;
import com.dingsns.start.user.model.UserProfileInfo;

public class ServiceTestServlet extends HttpServlet {

	private static final long serialVersionUID = -112210702214857712L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		final UserInfoService userInfoService = (UserInfoService) ApplicationContextHolder.ctx
				.getBean("userInfoService");
		UserProfileInfo user = userInfoService.fetchUserById(5l);
		resp.getWriter().print(user);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

}
